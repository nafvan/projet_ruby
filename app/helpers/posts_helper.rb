module PostsHelper
	#helper post

	#fonction qui affiche les likes s'il y en a moins de 8 on affiche le nombre de like sinon les noms des personnes
	def display_likes(post)
	votes = post.votes_for.up
	return list_likers(votes) if  votes.size <= 8
    		count_likers(votes)
    	end
  	end

  	#fonction qui retourne la class du like selon si le user like ou non le post
  	def liked_post(post)
	return 'glyphicon-heart' if current_user.voted_for? post
		'glyphicon-heart-empty'
	end

	private

	#fonction qui retourne la liste des personnes qui like un post
	def list_likers(votes)
	user_names = []
	unless votes.blank?
		  votes.voters.each do |voter|
		    user_names.push(link_to voter.user_name,
		    profile_path(voter.user_name),
		    class: 'user-name')
		  end
		  user_names.to_sentence.html_safe + like_plural(votes)
		end
	end

	#fonction qui affiche le nombre de likes sur un post
	def count_likers(votes)
	    vote_count = votes.size
	    vote_count.to_s + ' likes'
	end

	#change la phrase selon s'il y a un ou plusieurs qui aime le post
	def like_plural(votes)
	return ' like this' 
		if votes.count > 1
			' likes this'
	end
end




