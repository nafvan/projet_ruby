class PostsController < ApplicationController
	 before_action :authenticate_user!
	 before_action :owned_post, only: [:edit, :update, :destroy]
	 before_action :set_post, only: [:show, :edit, :update, :destroy, :like, :unlike]
	

	def index
		#affichage des post dans l'ordre décroissant à leur date de publication
		@posts = Post.all.order('created_at DESC')
	end

	def new
	  @post = current_user.posts.build
	end

	def create
     @post = current_user.posts.build(post_params)

	    if @post.save
	      flash[:success] = "Le post a été crée"
	      redirect_to root_path
	    else
	      flash[:alert] = "Votre post n'a pas été crée, veuillez vérifier le formulaire de post"
	      render :new
	    end
  	end

  	#méthode qui vérifie si le post appartient au user actuel
  	def owned_post
		@post=Post.find(params[:id])
		unless current_user.email == @post.user.email
			flash[:alert] = "Ce post n'est pas à vous" 
			redirect_to root_path
		end
	end

	def show
	end

	def edit
	end

	#méthode qui modifie le post
	def update
	    if @post.update(post_params)
	      flash[:success] = "Post mis à jour"
	      redirect_to root_path
	    else
	      flash.now[:alert] = "Mis à jour impossible, veuillez vérifier vos champs."
	      render :edit
	    end
	end
	
	#méthode qui permet de like un post=> un appel AJAX se fait pour le like, quand on "like"
	# la maj se fait sans rafraichissement de la page
	def like
		if @post.liked_by current_user
	      respond_to do |format|
	        format.html { redirect_to root_path }
	        format.js
	      end
	    end
	end

	#méthode qui retire le like d'un post
	def unlike
		if @post.unliked_by current_user
	      respond_to do |format|
	        format.html { redirect_to root_path }
	        format.js
	      end
	    end
	end

	def destroy
	  @post.destroy
	  redirect_to root_path
	end

	private

	def post_params
	  params.require(:post).permit(:image, :legende)
	end

	def set_post
	    @post = Post.find(params[:id])
	end

end
