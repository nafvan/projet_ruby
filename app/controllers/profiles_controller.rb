class ProfilesController < ApplicationController
	#controle des profiles
	before_action :authenticate_user!
	before_action :owned_profile, only: [:edit, :update]


	def show
		@user = User.find_by(user_name: params[:user_name])
		@posts = User.find_by(user_name: params[:user_name]).posts.order('created_at DESC')
	end

	def edit
	  @user = User.find_by(user_name: params[:user_name])
	end
	#méthode qui met à jour le profil apres modification
	def update
		@user = User.find_by(user_name: params[:user_name])
	    if @user.update(profile_params)
	      flash[:success] = 'Votre profile a été mis à jour'
	      redirect_to profile_path(@user.user_name)
	    else
	      @user.errors.full_messages
	      flash[:error] = @user.errors.full_messages
	      render :edit
	    end
	end
	#méthode qui vérifie si le profil correspond bien à celui qui tente de modifier
	def owned_profile
	    @user = User.find_by(user_name: params[:user_name])
	    unless current_user == @user
	      flash[:alert] = "Hey! ce n'est pas ton profile!"
	      redirect_to root_path
	    end
	end

	private

	def profile_params
	  params.require(:user).permit(:avatar, :bio)
	end

	before_action :set_user
	
	def set_user
	  @user = User.find_by(user_name: params[:user_name])
	 end
end
