class CommentsController < ApplicationController
	#controleur des commentaires
	before_action :set_post
	# méthode creation d'un commenaitre
	def create
		#instance commentaire 
	  @comment = @post.comments.build(comment_params)
	  @comment.user_id = current_user.id
	  	#quand on poste un commentaire on fait appel à un appel ajax pour l'afficher dynamiquement
		if @comment.save
		  respond_to do |format|
		    format.html { redirect_to root_path }
		    format.js
		  end
		else
		  flash[:alert] = "Le commentaire semble avoir un problème..."
		  render root_path
		end
	end

	#méthode supprimer un commentaire
	def destroy

    @comment = @post.comments.find(params[:id])
    #seuls les commentaires appartenant au user connecté au site peuvent supprimer
	    if @comment.user_id == current_user.id
	      @comment.delete
	      respond_to do |format|
	        format.html { redirect_to root_path }
	        format.js
	      end
	    end
	 end

	private

	def comment_params
	  params.require(:comment).permit(:content)
	end

	def set_post
	  @post = Post.find(params[:post_id])
	end
end
